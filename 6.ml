(* 
    6. Find out whether a list is a palindrome. (easy)

# is_palindrome ["x"; "a"; "m"; "a"; "x"];;
- : bool = true
# not (is_palindrome ["a"; "b"]);;
- : bool = true
*)


let rec rev (xs: 'a list) : 'a list =
    match xs with
    | [] -> []
    | [x] -> [x]
    | x :: rest -> (rev rest) @ [x] 

let is_palindrome (xs: 'a list) : bool =
    xs = rev xs