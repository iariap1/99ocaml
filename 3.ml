(* Find the K'th element of a list. (easy) *)

(*
# at 3 ["a"; "b"; "c"; "d"; "e"];;
- : string option = Some "c"
# at 3 ["a"];;
- : string option = None
 *)


(* 
 at [1;2]

*)

let rec at (index: int) (xs:'a list) : 'a option =
    match xs with
    | [] -> None
    | [_] when index > 0 -> None
    | x :: _ when index == 1 -> Some x
    | _ :: rest when index > 1  -> at (index - 1) rest