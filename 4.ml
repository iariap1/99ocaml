(*
 4. Find the number of elements of a list. (easy)
 
# length ["a"; "b"; "c"];;
- : int = 3
# length [];;
- : int = 0
*)


let rec length (xs: 'a list ) : int =
    match xs with
    | [] -> 0
    | [_] -> 1
    | x :: rest -> 1 + length rest