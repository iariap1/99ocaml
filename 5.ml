(*
5. Reverse a list. (easy)

# rev ["a"; "b"; "c"];;
- : string list = ["c"; "b"; "a"]
*)


let rec rev (xs: 'a list) : 'a list =
    match xs with
    | [] -> []
    | [x] -> [x]
    | x :: rest -> (rev rest) @ [x] 
